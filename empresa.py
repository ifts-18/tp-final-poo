from peewee import AutoField, CharField
from base_model import BaseModel

class Empresa(BaseModel):
    id = AutoField()
    cuit = CharField(max_length=11)
    razon_social = CharField(max_length=100)

    class Meta:
        table_name = 'empresas'

    def obtener_empresas():
        empresas = []
        empresas_en_bd = Empresa.select(Empresa.id, Empresa.razon_social, Empresa.cuit)
        for empresa in empresas_en_bd:
            plantilla = {
                'id': empresa.id,
                'cuit': empresa.cuit,
                'razon_social': empresa.razon_social
            }
            empresas.append(plantilla)
        return empresas