from peewee import AutoField, CharField, FloatField, ForeignKeyField
from base_model import BaseModel
from empresa import Empresa
from licitacion import Licitacion

class Adjudicacion(BaseModel):
    id = AutoField()
    monto_contrato = FloatField()
    empresa = ForeignKeyField(Empresa,backref='adjudicaciones')
    licitacion = ForeignKeyField(Licitacion,backref='adjudicaciones')
    expediente_numero = CharField()

    class Meta:
        table_name = 'adjudicaciones'