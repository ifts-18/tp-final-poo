from peewee import AutoField, CharField, ForeignKeyField, IntegerField
from base_model import BaseModel
from tipo_de_contratacion import TipoDeContratacion

class Licitacion(BaseModel):
    id = AutoField()
    tipo_contratacion = ForeignKeyField(TipoDeContratacion, backref='licitaciones')
    nro_contratacion = CharField(max_length=50)
    anio = IntegerField()

    class Meta:
        table_name = 'licitaciones'