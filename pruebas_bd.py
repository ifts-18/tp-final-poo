from obra import Obra
from tipo_de_obra import TipoDeObra
from area_responsable import AreaResponsable
from etapa import Etapa
from barrio import Barrio
from comuna import Comuna
from ubicacion import Ubicacion
from entorno import Entorno

from gestionar_obra_publica import GestionarObraPublica



if False:
    salud = TipoDeObra(nombre='Salud')
    educacion = TipoDeObra(nombre='Educacion')
    vivienda = TipoDeObra(nombre='Vivienda')

    area_salud = AreaResponsable(nombre='Area Salud')
    area_educacion = AreaResponsable(nombre='Area Educacion')
    area_vivienda = AreaResponsable(nombre='Area Vivienda')

    etapa_proyecto = Etapa(nombre='Proyecto')
    comuna_12 = Comuna(numero=12)
    barrio_villa_urquiza = Barrio(nombre='Villa Urquiza', comuna=comuna_12)

    ubicacion1 = Ubicacion(direccion='Mendoza 1000', barrio=barrio_villa_urquiza)

    entorno1 = Entorno(nombre='Villa Urquiza')

    salud.save()
    educacion.save()
    vivienda.save()
    area_salud.save()
    area_educacion.save()
    area_vivienda.save()
    etapa_proyecto.save()
    comuna_12.save()
    barrio_villa_urquiza.save()
    ubicacion1.save()
    entorno1.save()


# nueva_obra1 = Obra(
#     nombre='Panchería con salsas', 
#     descripcion='Los mejores panchos de villurca',
#     tipo_de_obra = educacion,
#     entorno=entorno1,
#     area_responsable=area_educacion,
#     etapa=etapa_proyecto,
#     ubicacion=ubicacion1
#     )

# nueva_obra2 = Obra(
#     nombre='Cafetería los Erizos', 
#     descripcion='Venta de café',
#     tipo_de_obra = tipo_elegido,
#     entorno='Villa Urquiza',
#     area_responsable='Python',
#     etapa='Proyecto',
#     ubicacion='nada',
#     adjudicacion='Sin definir',
#     financiamiento='Sin definir'
#     )

#print('OBRA:', nueva_obra1)
#nueva_obra1.save()
#nueva_obra2.save()

#consulta = Obra.select().where(Obra.tipo_de_obra == 2).order_by(Obra.nombre)
#print('consulta:', consulta)


# interacción de uso para el menú
# llamada al método funciona correctamente
nombre_elegido = 'Plaza para gatitos'
descripcion_elegida = 'Proyecto de plaza para gatitos'
tipo_elegido = TipoDeObra.select().where(TipoDeObra.id == 1).first()
entorno_elegido = Entorno.select().where(Entorno.id == 1).first()
area_responsable_elegida = AreaResponsable.select().where(AreaResponsable.id == 3).first()
ubicacion_elegida = Ubicacion.select().where(Ubicacion.id == 1).first()

#print(tipo_elegido.nombre)

GestionarObraPublica.nueva_obra(
    nombre=nombre_elegido, 
    descripcion=descripcion_elegida,
    tipo_de_obra = tipo_elegido,
    entorno=entorno_elegido,
    area_responsable=area_responsable_elegida,
    ubicacion=ubicacion_elegida
    )

# nueva_obra = Obra(
#     nombre=nombre_elegido, 
#     descripcion=descripcion_elegida,
#     tipo_de_obra = tipo_elegido,
#     entorno=entorno_elegido,
#     area_responsable=area_responsable_elegida,
#     ubicacion=ubicacion_elegida
#     )

# print(nueva_obra.nombre)