from bd import bd

#from modelo_orm import BaseModel
#from modelo_orm import AreaResponsable
from modelo_orm import AreaResponsable
from modelo_orm import Ubicacion
from gestionar_obras import GestionarObra

try:
    GestionarObra.conectar_db()
except:
    print('Se ha generado un error en la conexión a la BD.')

try:
    bd.create_tables([
        #Adjudicacion, 
        AreaResponsable,
        #Barrios,
        #Comuna,
        #Empresa,
        #Entorno,
        #Etapa,
        #Licitacion,
        #Obra,
        #TipoDeContratacion,
        #TipoDeFinanciamiento,
        #TipoDeObra,
        Ubicacion])
except:
    print('Algo falló al crear las tablas.')