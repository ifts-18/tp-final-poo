import sqlite3
import pandas as pd
import bd as base_datos
from peewee import *
from constantes import *
from gestionar_obra import GestionarObra
from modelo_orm import *
from constantes import BARRIOS, AREAS_RESPONSABLES, TIPOS_DE_OBRAS
# from area_responsable import AreaResponsable
# from barrio import Barrio
# from entorno import Entorno
# from ubicacion import Ubicacion
# from tipo_de_obra import TipoDeObra
# from obra import Obra

# Enlistar opciones
def mostrar_informacion(LIST, text:str):
    print('# Ingrese el numero que corresponda', text)
    for item in LIST:
        opcion = item['opcion']
        nombre = item['nombre']
        print(f'{opcion}: {nombre}')
    opcion_elegida = int(input(f'Opcion: '))
    return opcion_elegida

class GestionarObraPublica(GestionarObra):

    __df: pd.DataFrame

    @classmethod
    def extraer_datos(cls, archivo_csv: str, columnas_elegidas: list):
        try:
            cls.__df = pd.read_csv(archivo_csv, usecols=columnas_elegidas)
        except FileNotFoundError as e:
            print('Error al conectar con el dataset.', e)

    @classmethod
    def conectar_db(cls):
        try:
            base_datos.bd.connect()
            print(f'BD {base_datos.NOMBRE}: CONEXIÓN ESTABLECIDA.')
        except:
            print(f'BD {base_datos.NOMBRE}: CONEXIÓN FALLIDA.')

    @classmethod
    def mapear_orm(cls, tablas: list):
        try:
            base_datos.bd.create_tables(tablas)
        except:
            print('Algo falló al crear las tablas.')

    @classmethod
    def limpiar_datos(cls):
        
                cls.__df.dropna(subset=['comuna'], inplace=True)
                cls.__df.dropna(subset=['barrio'],inplace=True)
                cls.__df.dropna(subset=['direccion'],inplace=True)
                cls.__df.dropna(subset=['entorno'],inplace=True)
                cls.__df.dropna(subset=['etapa'],inplace=True)
                cls.__df.dropna(subset=['cuit_contratista'],inplace=True)
                cls.__df.dropna(subset=['licitacion_oferta_empresa'],inplace=True)
                cls.__df.dropna(subset=['contratacion_tipo'],inplace=True)
                cls.__df.dropna(subset=['tipo'],inplace=True)
                cls.__df['destacada'].fillna('NULL', inplace=True)#lo manejo de esta manera ya que me elimino muchos registros con dropna
                cls.__df.dropna(subset=['nombre'],inplace=True)
                cls.__df.dropna(subset=['descripcion'],inplace=True) 
                cls.__df.dropna(subset=['fecha_inicio'],inplace=True)
                cls.__df.dropna(subset=['fecha_fin_inicial'],inplace=True)
                cls.__df.dropna(subset=['plazo_meses'],inplace=True)
                cls.__df.dropna(subset=['porcentaje_avance'],inplace=True)
                cls.__df['mano_obra'].fillna('NULL', inplace=True)
                cls.__df.dropna(subset=['area_responsable'],inplace=True)
                cls.__df.dropna(subset=['monto_contrato'],inplace=True)
                cls.__df['expediente-numero'].fillna('NULL', inplace=True)
                cls.__df.dropna(subset=['licitacion_anio', 'nro_contratacion'], inplace=True)
                cls.__df['financiamiento'].fillna('NULL', inplace=True)



    @classmethod
    def cargar_datos(cls):

        # Comunas 
            data_unique_comuna = list(cls.__df['comuna'].unique())
            print(data_unique_comuna)
            for elem in data_unique_comuna:
               print("Elemento:", elem)
               try:
                Comuna.create(numero=elem)
               except IntegrityError as e:
                print("Error al insertar un nuevo registro en la tabla tipos_comunas.", e)
            print("Se han persistido los tipos de transporte en la BD.")

        # #entorno
            data_unique_tipoFinanciamiento = list(cls.__df['entorno'].unique())
            print(data_unique_tipoFinanciamiento)
            for elem in data_unique_tipoFinanciamiento:
               print("Elemento:", elem)
               try:
                Entorno.create(nombre=elem)
               except sqlite3.IntegrityError as e:
                print("Error al insertar un nuevo registro en la tabla tipo_Entorno.", e)
            print("Se han persistido los tipos de transporte en la BD.")
        
        # #Etapas 
            data_unique_Etapa = list(cls.__df['etapa'].unique())
            print(data_unique_Etapa)
            for elem in data_unique_Etapa:
               print("Elemento:", elem)
               try:
                Etapa.create(nombre=elem)
               except sqlite3.IntegrityError as e:
                print("Error al insertar un nuevo registro en la tabla tipos_etapas.", e)
            print("Se han persistido los tipos de transporte en la BD.")    
        
        # #Tipo de Obra
            data_unique_tipo = list(cls.__df['tipo'].unique())
            print(data_unique_tipo)
            for elem in data_unique_tipo:
               print("Elemento:", elem)
               try:
                TipoDeObra.create(nombre=elem)
               except sqlite3.IntegrityError  as e:
                print("Error al insertar un nuevo registro en la tabla tipos_de_obra.", e)
            print("Se han persistido los tipos de transporte en la BD.")
        
        # #Area responsable
            data_unique_AreaResponsable = list(cls.__df['area_responsable'].unique())
            print(data_unique_AreaResponsable)
            for elem in data_unique_AreaResponsable:
               print("Elemento:", elem)
               try:
                AreaResponsable.create(nombre=elem)
               except sqlite3.IntegrityError as e:
                print("Error al insertar un nuevo registro en la tabla area_responsable.", e)
            print("Se han persistido los tipos de transporte en la BD.")
        
        # #Tipo de Contratacion 
            data_unique_tipoContraracion = list(cls.__df['contratacion_tipo'].unique())
            print(data_unique_tipoContraracion)
            for elem in data_unique_tipoContraracion:
               print("Elemento:", elem)
               try:
                TipoDeContratacion.create(nombre=elem)
               except sqlite3.IntegrityError as e:
                print("Error al insertar un nuevo registro en la tabla tipo_de_contratacion.", e)
            print("Se han persistido los tipos de transporte en la BD.")
        
        # #Tipo de Financiamiento 
            data_unique_tipoFinanciamiento = list(cls.__df['financiamiento'].unique())
            print(data_unique_tipoFinanciamiento)
            for elem in data_unique_tipoFinanciamiento:
               print("Elemento:", elem)
               try:
                TipoDeFinanciamiento.create(nombre=elem)
               except sqlite3.IntegrityError as e:
                print("Error al insertar un nuevo registro en la tabla tipos_financimaiento.", e)
            print("Se han persistido los tipos de transporte en la BD.")

        # #Barrios
            data_unique_barrios = list(cls.__df['barrio'].unique())
            for elem in data_unique_barrios:
                print("Elemento:", elem)
                # Obtener el primer número de comuna asociado al barrio
                comuna_numero = cls.__df.loc[cls.__df['barrio'] == elem, 'comuna'].iloc[0]
                try:
                    # Buscar la comuna en la tabla Comunas
                    comunas = Comuna.get(Comuna.numero == comuna_numero)
                    # Crear un nuevo registro en la tabla Barrios con la comuna asociada
                    Barrio.create(nombre=elem, comuna=comunas)
                    print(f"Se ha insertado el barrio {elem} en la tabla Barrios.")
                except Comuna.DoesNotExist:
                    print(f"No se encontró la comuna con el número {comuna_numero} en la tabla Comunas.")
                except sqlite3.IntegrityError as e:
                    print(f"Error al insertar el barrio {elem} en la tabla Barrios. {e}")
        
        # #direccion
            data_unique_direccion = list(cls.__df['direccion'].unique())
            for elem in data_unique_direccion:
                print("Elemento:", elem)
                # Obtener el primer número de barrio asociado a la direccion
                direccion_numero = cls.__df.loc[cls.__df['direccion'] == elem, 'barrio'].iloc[0]
                try:
                    # Buscar el nombre barrio asociado con la direccion_numero
                    barrios = Barrio.get(Barrio.nombre == direccion_numero)
                    # Crear un nuevo registro en la tabla Ubicacion
                    Ubicacion.create(direccion=elem, barrio=barrios)
                    print(f"Se ha insertado la  direccion {elem} en la tabla Ubicacion.")
                except Barrio.DoesNotExist:
                    print(f"No se encontró al barrio {direccion_numero} en la tabla Ubicacion.")
                except sqlite3.IntegrityError as e:
                    print(f"Error al insertar la direccion {elem} en la tabla Ubicacion. {e}")
        
        
        #licitaciones
            data_unique_licitacion = cls.__df[['licitacion_anio', 'nro_contratacion']].drop_duplicates()

            for index, row in data_unique_licitacion.iterrows():
                licitacion_anio = row['licitacion_anio']
                nro_contratacion = row['nro_contratacion']
                print(f"Licitación año: {licitacion_anio}, Nro contratación: {nro_contratacion}")

                try:
                    # Obtener el primer número de TipoDeContratación asociado a 'licitacion_anio' y 'nro_contratacion'
                    filtro_licitacion = (cls.__df['licitacion_anio'] == licitacion_anio) & (cls.__df['nro_contratacion'] == nro_contratacion)

                    if not cls.__df.loc[filtro_licitacion, 'contratacion_tipo'].empty:
                        primer_numero_contratacion = cls.__df.loc[filtro_licitacion, 'contratacion_tipo'].iloc[0]

                        try:
                            # Buscar el Tipo de Contratación en la tabla TipoDeContratacion
                            tipo_contratacion = TipoDeContratacion.get(TipoDeContratacion.nombre == primer_numero_contratacion)

                            # Obtener o crear la instancia de Licitacion con el tipo_contratacion asociado
                            licitacion, created = Licitacion.get_or_create(
                                tipo_contratacion=tipo_contratacion,
                                nro_contratacion=nro_contratacion,
                                anio=licitacion_anio
                            )

                            if created:
                                print(f"Licitación Anio: {licitacion_anio}, Nro de Contratación: {nro_contratacion}, Se han creado agregado como registro en la tabla de Licitacion.")
                            else:
                                print(f"La Licitación con Anio: {licitacion_anio}, Nro de Contratación: {nro_contratacion} ya existe.")
                                
                        except TipoDeContratacion.DoesNotExist:
                            print(f"No se encontró el tipo de contratación: {primer_numero_contratacion}")

                except Licitacion.DoesNotExist:
                    # Si no existe, imprimir un mensaje indicando que no se crea una nueva instancia
                    print(f"No se encontró la licitación con el año y número de contratación proporcionados.")
                    
                except IntegrityError as e:
                    # Si hay una violación de integridad (por ejemplo, clave única), manejarlo según tus necesidades
                    print(f"Error de integridad: {e}")

                except Exception as e:
                    print(f"Error: {e}")

            print("Se han persistido los datos en la BD.")
                                                                    
                            
                              
         
        # #empresas
            data_unique_cuit = list(cls.__df['cuit_contratista'].unique())
            data_unique_razon_social = list(cls.__df['licitacion_oferta_empresa'])

            for cuits, razon_sociales in zip(data_unique_cuit, data_unique_razon_social):
                print(f"Cuit: {cuits}, Razon Social: {razon_sociales}")

                try:
                    empresa, created = Empresa.get_or_create(razon_social=razon_sociales, defaults={'cuit': cuits})
                    if created:
                        print("Empresa creada correctamente.")
                    else:
                        print(f"La empresa con Razon Social: {razon_sociales} ya existe. Actualizando información si es necesario.")
                        # Puedes agregar aquí la lógica para actualizar otros campos si es necesario
                except IntegrityError as e:
                    print("Error al insertar o actualizar un registro en la tabla Empresas.", e)

            print("Se han persistido los datos en la BD.")                
       
        
        # # #adjudicaciones
            data_unique_monto_contrato = list(cls.__df['monto_contrato'].unique())
            data_unique_expediente_numero = list(cls.__df['expediente-numero'].unique())

            for monto_contratos, expediente_numeros in zip(data_unique_monto_contrato, data_unique_expediente_numero):
                        print(f"Monto Contrato: {monto_contratos}, Expediente Numero: {expediente_numeros}")

                        try:
                                # Obtener el primer numero de empresa_id asociado a 'monto_contrato' y 'expediente_numero'
                                filtro_empresa = (cls.__df['monto_contrato'] == monto_contratos) & (cls.__df['expediente-numero'] == expediente_numeros)

                                if not cls.__df.loc[filtro_empresa, 'cuit_contratista'].empty:
                                        empresa_cuit = cls.__df.loc[filtro_empresa, 'cuit_contratista'].iloc[0]

                                # Verificar que el CUIT sea válido
                                if pd.notna(empresa_cuit):
                                        empresas = Empresa.get(Empresa.cuit == empresa_cuit)

                                        # Obtener el primer numero de licitacion_id asociado a 'monto_contrato' y 'expediente_numero'
                                        filtro_licitacion = (cls.__df['monto_contrato'] == monto_contratos) & (cls.__df['expediente-numero'] == expediente_numeros)

                                        if not cls.__df.loc[filtro_licitacion, 'licitacion_anio'].empty:
                                                licitacion_anio = cls.__df.loc[filtro_licitacion, 'licitacion_anio'].iloc[0]                               
                                        
                                        filtro_contratacion = (cls.__df['monto_contrato'] == monto_contratos) & (cls.__df['expediente-numero'] == expediente_numeros)

                                        if not cls.__df.loc[filtro_contratacion, 'nro_contratacion'].empty:
                                                nro_contratacion = cls.__df.loc[filtro_contratacion, 'nro_contratacion'].iloc[0]
                                        try:
                                                licitaciones = Licitacion.get(Licitacion.anio == licitacion_anio, Licitacion.nro_contratacion == nro_contratacion)

                                                # Crear un nuevo registro en la tabla Adjudicacion
                                                Adjudicacion.create(
                                                monto_contrato=monto_contratos,
                                                empresa=empresas,
                                                licitacion=licitaciones,
                                                expediente_numero=expediente_numeros
                                                )

                                                print("Se ha creado agregado un registro en la tabla Adjudicacion.")
                                        except Licitacion.DoesNotExist:
                                                print(f"No se encontró la licitación con el año {licitacion_anio} y el número de contratación {nro_contratacion}.")
                                        except sqlite3.IntegrityError as e:
                                                print("Error al insertar un nuevo registro en la tabla Adjudicacion.", e)
                                        else:
                                                print("Error: CUIT no válido.")
                        except sqlite3.IntegrityError as e:
                                        print("Error al insertar un nuevo registro en la tabla Adjudicacion.", e)

                        print("Se han persistido los datos en la BD.")

        # # #obra
            for index, row in cls.__df.iterrows():
                                try:
                                        # Obttengo de las FK las coincidencias con el Dataframe
                                        tipo_de_obra, _ = TipoDeObra.get_or_create(nombre=row['tipo'])
                                        entorno, _ = Entorno.get_or_create(nombre=row['entorno'])
                                        area_responsable, _ = AreaResponsable.get_or_create(nombre=row['area_responsable'])
                                        etapa, _ = Etapa.get_or_create(nombre=row['etapa'])

                                        #para Adjudicacion
                                        expediente_numero = row['expediente-numero']
                                        monto_contratos = row['monto_contrato']
                                        try:
                                                monto_contrato = float(monto_contratos.replace('.', '').replace('$', '').replace(',', '')) if pd.notna(monto_contratos) else None #convierto en flotanto este campo y elimino los simbolos (.$,)
                                        except ValueError:
                                                print(f"Error: No se puede convertir '{monto_contratos}' a un número.")
                                                continue

                                        cuit_empresa = row['cuit_contratista']
                                        licitacion_anio = row['licitacion_anio']
                                        nro_contratacion = row['nro_contratacion']
                                        print(f"Índice: {index}, Año de licitación: {licitacion_anio}, Número de contratación: {nro_contratacion}")

                                        # trato el primer número de licitacion_id y nro_contratacion asociado a 'monto_contrato' y 'expediente_numero'
                                        try:
                                                licitacion_anio = cls.__df.loc[(cls.__df['monto_contrato'] == monto_contratos) & (cls.__df['expediente-numero'] == expediente_numero), 'licitacion_anio'].iloc[0]
                                                nro_contratacion = cls.__df.loc[(cls.__df['monto_contrato'] == monto_contratos) & (cls.__df['expediente-numero'] == expediente_numero), 'nro_contratacion'].iloc[0]
                                        except IndexError:
                                                print(f"Error: No se encontró licitación para el índice {index}")
                                                continue

                                        try:
                                                nro_licitaciones = Licitacion.get(Licitacion.anio == licitacion_anio, Licitacion.nro_contratacion == nro_contratacion)

                                                if pd.notna(expediente_numero) and pd.notna(cuit_empresa):
                                                        # Obtener de Adjudicacion con la coincidencia de sus correspondientes campos
                                                        adjudicacion, _ = Adjudicacion.get_or_create(
                                                        expediente_numero=expediente_numero,
                                                        monto_contrato=monto_contrato,
                                                        empresa=cuit_empresa,
                                                        licitacion=nro_licitaciones
                                                        )

                                                        financiamiento, created = TipoDeFinanciamiento.get_or_create(nombre=row['financiamiento'])

                                                        # para ubicación
                                                        numero_direccion = row['direccion']
                                                        nombre_barrio = row['barrio']
                                                        ubicacion, _ = Ubicacion.get_or_create(direccion=numero_direccion, barrio=nombre_barrio)

                                                        # Valido existencia de registros relacionados
                                                        if not tipo_de_obra or not entorno or not area_responsable or not etapa or not ubicacion or not adjudicacion or not financiamiento:
                                                                print("Error: Al menos una instancia relacionada no existe.")
                                                                continue

                                                        # Crear un nuevo registro en la tabla Obra
                                                        Obra.create(
                                                        nombre=row['nombre'],
                                                        descripcion=row['descripcion'],
                                                        mano_obra=row['mano_obra'],
                                                        porcentaje_avance=row['porcentaje_avance'],
                                                        destacada=row['destacada'],
                                                        tipo_de_obra=tipo_de_obra,
                                                        fecha_inicio=row['fecha_inicio'],
                                                        fecha_fin_inicial=row['fecha_fin_inicial'],
                                                        plazo_meses=row['plazo_meses'],
                                                        entorno=entorno,
                                                        area_responsable=area_responsable,
                                                        etapa=etapa,
                                                        ubicacion=ubicacion,
                                                        adjudicacion=adjudicacion,
                                                        financiamiento=financiamiento
                                                        )

                                                        print(f"Se ha creado una nueva instancia de Obra para el índice {index}.")

                                        except Licitacion.DoesNotExist:
                                                print(f"No se encontró la licitación con el año {licitacion_anio} y el número de contratación {nro_contratacion}.")

                                except Exception as e:
                                        print(f"Error al insertar un nuevo registro en la tabla Obra: {e}")

                                print("Se han persistido los datos en la BD.")           
                
    @classmethod
    def __solicitar_nombre(cls):
        nombre = str(input('# Ingrese el nombre del proyecto: '))
        while nombre.lstrip() == '' or nombre.isnumeric():
            print('Ingreso incorrecto. Ingrese un nombre')
            nombre = str(input('# Ingrese el nombre del proyecto: '))
        return nombre
    
    @classmethod
    def __solicitar_descripcion(cls):
        descripcion = str(input('# Ingrese la descripción: '))
        while descripcion.lstrip() == '' or descripcion.isnumeric():
            print('Ingreso incorrecto. Ingrese una descripción')
            descripcion = str(input('# Ingrese la descripción: '))
        return descripcion
    
    @classmethod
    def __solicitar_direccion(cls):
        direccion = str(input('# Ingrese la direccion: '))
        while direccion.lstrip() == '' or direccion.isnumeric():
            print('Ingreso incorrecto. Ingrese una direcccion')
            direccion = str(input('# Ingrese la direccion: '))
        return direccion
    
    @classmethod
    def __solicitar_barrio(cls):
        barrios = Barrio.obtener_barrios()
        for barrio in barrios:
            id = barrio['id']
            nombre = barrio['nombre']
            print(f'{id} {nombre}')
        opcion_ingresada = int(input('Opción: '))
        registro_barrio = Barrio.select().where(Barrio.id == opcion_ingresada).first()
        return registro_barrio

    @classmethod
    def __crear_ubicacion(cls, direccion: str, barrio: Barrio):
        try:
            nueva_ubicacion = Ubicacion(direccion=direccion, barrio=barrio)
            nueva_ubicacion.save()
        except:
            print('OPCIÓN INGRESADA NO EXISTE - INGRESAR NUEVO REGISTRO')

    @classmethod
    def __solicitar_area_responsable(cls):
        areas = AreaResponsable.obtener_areas_responsables()
        for area in areas:
            id = area['id']
            nombre = area['nombre']
            print(f'{id} {nombre}')
        opcion_ingresada = int(input('Opción: '))
        registro_area = AreaResponsable.select().where(AreaResponsable.id == opcion_ingresada).first()
        return registro_area
    
    @classmethod
    def __solicitar_tipo_de_obra(cls):
        tipos_de_obra = TipoDeObra.obtener_tipos_de_obra()
        for tipo_de_obra in tipos_de_obra:
            id = tipo_de_obra['id']
            nombre = tipo_de_obra['nombre']
            print(f'{id} {nombre}')
        opcion_ingresada = int(input('Opción: '))
        registro_tipo_de_obra = TipoDeObra.select().where(TipoDeObra.id == opcion_ingresada).first()
        return registro_tipo_de_obra
    
    @classmethod
    def __solicitar_entorno(cls):
        entornos = Entorno.obtener_entornos()
        for entorno in entornos:
            id = entorno['id']
            nombre = entorno['nombre']
            print(f'{id} {nombre}')
        opcion_ingresada = int(input('Opción: '))
        registro_entorno = Entorno.select().where(Entorno.id == opcion_ingresada).first()
        return registro_entorno

    @classmethod
    def nueva_obra(cls):
            nombre = cls.__solicitar_nombre()
            descripcion = cls.__solicitar_descripcion()
            registro_barrio = cls.__solicitar_barrio()
            direccion = cls.__solicitar_direccion()
            cls.__crear_ubicacion(direccion=direccion, barrio=registro_barrio)
            ubicacion = Ubicacion.select().where(Ubicacion.direccion == direccion)
            area_responsable = cls.__solicitar_area_responsable()
            tipo_de_obra = cls.__solicitar_tipo_de_obra()
            entorno = cls.__solicitar_entorno()

            nueva_obra = Obra.nuevo_proyecto(
                nombre=nombre, 
                descripcion=descripcion,
                tipo_de_obra = tipo_de_obra,
                entorno=entorno,
                area_responsable=area_responsable,
                ubicacion=ubicacion
                )
            nueva_obra.save()

    @classmethod
    def obtener_indicadores(cls):
                raise 'ERROR: obtener_indicadores no implementado'
                        
    @classmethod
    def ver_dataframe(cls):
        try:
            return cls.__df
        except:
            print('No existe el dataframe.')


def crear_bd():     
    GestionarObraPublica.conectar_db()
    GestionarObraPublica.mapear_orm(tablas)
    GestionarObraPublica.extraer_datos(DATASET,COLUMNAS_ELEGIDAS)
    GestionarObraPublica.limpiar_datos()
    GestionarObraPublica.cargar_datos()