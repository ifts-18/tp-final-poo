from area_responsable import AreaResponsable
from tipo_de_obra import TipoDeObra
from obra import Obra
from peewee import *
from etapa import Etapa

# areas = AreaResponsable.ver_areas_responsables()
# for area in areas:
#     print(area)

# tipos_obras = TipoDeObra.ver_tipo_de_obras()
# for tipo in tipos_obras:
#     print(tipo)

# Cantidad de obras que se encuentran en cada etapa
def etapas_obras():
    resultado = {}
    obras_por_etapa = (
        Obra
        .select(Obra.etapa_id, fn.COUNT(Obra.id).alias('cantidad'))
        .group_by(Obra.etapa_id)
    )

    for obra_etapa in obras_por_etapa:
        nombre_etapa = Etapa.get(Etapa.id == obra_etapa.etapa_id).nombre
        resultado[nombre_etapa] = obra_etapa.cantidad

    for nombre_etapa, cantidad_obras in resultado.items():
        print(f'{nombre_etapa}: {cantidad_obras}')

#etapas_obras()

# Cantidad de obras y monto total de inversion por tipo de obra
# monto contrato

def monto_inversiones_por_tipo_obra():
    resultado = {}

    obras_por_tipo = (
        Obra
        .select(Obra.tipo_de_obra, fn.COUNT(Obra.id).alias('cantidad'))
        .group_by(Obra.tipo_de_obra)
    )

    for tipo in obras_por_tipo:
        tipo_obra = TipoDeObra.get(TipoDeObra.id == tipo.tipo_de_obra_id).nombre
        # resultado[tipo_obra] = 

    for tipo, cantidad_obras in resultado.items():
        print(f'{tipo_obra}: {cantidad_obras}')

monto_inversiones_por_tipo_obra()