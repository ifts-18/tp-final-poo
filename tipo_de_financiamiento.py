from peewee import AutoField, CharField
from base_model import BaseModel

class TipoDeFinanciamiento(BaseModel):
    id = AutoField()
    nombre = CharField(max_length=100)

    class Meta:
        table_name = 'tipos_de_financiamiento'