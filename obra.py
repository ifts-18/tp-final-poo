from adjudicacion import Adjudicacion
from area_responsable import AreaResponsable
from base_model import BaseModel
from entorno import Entorno
from etapa import Etapa
from licitacion import Licitacion
from peewee import *
from tipo_de_contratacion import TipoDeContratacion
from tipo_de_financiamiento import TipoDeFinanciamiento
from tipo_de_obra import TipoDeObra
from ubicacion import Ubicacion
from empresa import Empresa


class Obra(BaseModel):
    id = AutoField()
    nombre = CharField(max_length=300)
    descripcion = CharField(max_length=500)
    mano_obra = IntegerField(default=0)
    porcentaje_avance = FloatField(default=0)
    destacada = BooleanField(default=False)
    tipo_de_obra = ForeignKeyField(TipoDeObra, backref='obras')
    fecha_inicio = CharField(default='Sin definir') # Ver de usar tipo de dato Date
    fecha_fin_inicial = CharField(default='Sin definir') # Mismo caso fecha_inicio
    plazo_meses = IntegerField(4)
    entorno = ForeignKeyField(Entorno, backref='obras')
    area_responsable = ForeignKeyField(AreaResponsable, backref='obras')
    etapa = ForeignKeyField(Etapa, backref='obras')
    ubicacion = ForeignKeyField(Ubicacion, backref='obras')
    licitacion = ForeignKeyField(Licitacion, backref='obras', null=True)
    adjudicacion = ForeignKeyField(Adjudicacion, backref='obras', null=True)
    financiamiento = ForeignKeyField(TipoDeFinanciamiento, backref='obras', null=True)

    class Meta:
        table_name = 'obras'

    def ver_obras():
        obras = Obra.select(Obra.nombre, Obra.id)
        for obra in obras:
            print(f'{obra.id} {obra.nombre}')

    def obtener_obras():
        obras = []
        obras_en_bd = Obra.select(Obra.id, Obra.nombre)
        for obra in obras_en_bd:
            plantilla = {
                'id': obra.id,
                'nombre': obra.nombre
            }
            obras.append(plantilla)
        return obras
    
    def obtener_obra_por_nombre(nombre: str):
        try:
            obra = Obra.select().where(Obra.nombre == nombre).first()
            print(obra.nombre)
            return obra
        except AttributeError:
            print('VALOR NO ENCONTRADO')

    def nuevo_proyecto(
        nombre: str, 
        descripcion: str, 
        tipo_de_obra: TipoDeObra,
        entorno: Entorno,
        area_responsable: AreaResponsable,
        ubicacion: Ubicacion
        ):
            etapa_inicial = Etapa.select().where(Etapa.nombre == 'Proyecto').first()       
            return Obra(
                nombre = nombre, 
                descripcion = descripcion,
                tipo_de_obra=tipo_de_obra,
                entorno=entorno,
                area_responsable=area_responsable,
                ubicacion = ubicacion,
                etapa = etapa_inicial
                )

    def ver_obras_en_etapa_proyecto():
        etapa_inicial = Etapa.select().where(Etapa.nombre == 'Proyecto').first()
        try:
            obras = Obra.select().where(Obra.etapa == etapa_inicial)
            i = 1
            for obra in obras:
                print(f'{obra.id} - Etapa: {obra.nombre}')
                i += 1
        except:
            print('Etapa no encontrada')

    def solicitar_obra():
        Obra.ver_obras_en_etapa_proyecto()
        opcion_ingresada = int(input('Opción: '))
        registro_obra = Obra.select().where(Obra.id == opcion_ingresada).first()
        return registro_obra

    def solicitar_tipo_de_contratacion():
        tipos_de_contratacion = TipoDeContratacion.obtener_tipos_de_contratacion()
        for tipo_de_contratacion in tipos_de_contratacion:
            id = tipo_de_contratacion['id']
            nombre = tipo_de_contratacion['nombre']
            print(f'{id} {nombre}')
        opcion_ingresada = int(input('Opción: '))
        registro_tipo_de_contratacion = TipoDeContratacion.select().where(TipoDeContratacion.id == opcion_ingresada).first()
        return registro_tipo_de_contratacion

    def crear_licitacion(nro_contratacion: str):
        tipo_contratacion = Obra.solicitar_tipo_de_contratacion()
        anio = int(input('# Ingrese el año: '))
        nueva_licitacion = Licitacion(
            tipo_contratacion=tipo_contratacion, 
            nro_contratacion=nro_contratacion,
            anio=anio)
        nueva_licitacion.save()

    def iniciar_contratacion():
        obra_a_licitar = Obra.solicitar_obra()
        nro_contratacion = str(input('# Ingrese el numero de contratacion: '))
        Obra.crear_licitacion(nro_contratacion=nro_contratacion)
        nueva_licitacion = Licitacion.select().where(Licitacion.nro_contratacion == nro_contratacion).first()
        print('OBRA SELECCIONADA:', obra_a_licitar.nombre)
        obra_a_licitar.licitacion = nueva_licitacion
        obra_a_licitar.save()
        print('ID LICITACION SELECCIONADA:', obra_a_licitar.licitacion)
        
    def solicitar_empresa():
        empresas = Empresa.obtener_empresas()
        for empresa in empresas:
            id = empresa['id']
            cuit = empresa['cuit']
            razon_social = empresa['razon_social']
            print(f'ID: {id} - CUIT: {cuit} - Razón Social: {razon_social}')
        opcion_ingresada = int(input('Opción: '))
        registro_empresa = Empresa.select().where(Empresa.id == opcion_ingresada).first()
        return registro_empresa
    
    def adjudicar_obra():
        empresa = Obra.solicitar_empresa()
        nro_expediente = str(input('# Ingrese el numero de expediente: '))
        monto_contrato = float(input('# Ingrese el monto del contrato: '))
        obra_a_adjudicar = Obra.solicitar_obra()
        etapa_inicial = Etapa.select().where(Etapa.nombre == 'Proyecto').first()
        nueva_adjudicacion = Adjudicacion(monto_contrato=monto_contrato, )
        licitacion = Obra.select().where(Obra.licitacion == etapa_inicial).first()
        return etapa_inicial

    def iniciar_obra():
        obra = Obra.solicitar_obra()

        destacada = str(input('Ingrese SI o NO: ' ))
        fecha_inicio = str(input('Ingrese la fecha dd/mm/aaaa: '))
        fecha_fin_inicial = str(input('Ingrese la fecha de fin inicial dd/mm/aaaa: '))
        financiamiento = str(input('Ingrese la fuente de financiamiento: '))
        mano_obra = int(input('Ingrese la cantidad de mano de obra: '))

        obra.destacada = destacada
        obra.fecha_inicio = fecha_inicio
        obra.fecha_fin_inicial = fecha_fin_inicial
        obra.financiamiento = financiamiento
        obra.mano_obra = mano_obra
        obra.save()

    def actualizar_porcentaje_avance():
        obra = Obra.solicitar_obra()
        porcentaje_avance = int(input('Ingrese el porcentaje del avance: '))
        obra.porcentaje_avance = porcentaje_avance
        obra.save()

    def incrementar_plazo():
        obra = Obra.solicitar_obra()
        nuevo_plazo = int(input('Ingrese el nuevo plazo de la obra: '))
        obra.plazo_meses = nuevo_plazo
        obra.save()

    def incrementar_mano_obra():
        obra = Obra.solicitar_obra()
        nueva_mano_de_obra = int(input('Ingrese la nueva cantidad de mano de obra: '))
        obra.mano_obra = nueva_mano_de_obra
        obra.save()

    def finalizar_obra():
        obra = Obra.solicitar_obra()
        etapa_finalizada = Etapa.select().where(Etapa.id == 12).first()
        obra.etapa = etapa_finalizada
        obra.save()

    def rescindir_obra():
        obra = Obra.solicitar_obra()
        etapa_rescindida = Etapa.select().where(Etapa.id == 3).first()
        obra.etapa = etapa_rescindida
        obra.save()