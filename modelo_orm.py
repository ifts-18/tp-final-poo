from base_model import BaseModel
from adjudicacion import Adjudicacion
from area_responsable import AreaResponsable
from barrio import Barrio
from comuna import Comuna
from empresa import Empresa
from entorno import Entorno
from etapa import Etapa
from licitacion import Licitacion
from obra import Obra
from tipo_de_contratacion import TipoDeContratacion
from tipo_de_financiamiento import TipoDeFinanciamiento
from tipo_de_obra import TipoDeObra
from ubicacion import Ubicacion

tablas = [
    Adjudicacion, 
    AreaResponsable,
    Barrio,
    Comuna,
    Empresa,
    Entorno,
    Etapa,
    Licitacion,
    Obra,
    TipoDeContratacion,
    TipoDeFinanciamiento,
    TipoDeObra,
    Ubicacion
]