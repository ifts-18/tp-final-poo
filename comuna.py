from peewee import AutoField, IntegerField
from base_model import BaseModel

class Comuna(BaseModel):
    id = AutoField()
    numero = IntegerField()

    class Meta:
        table_name = 'comunas'