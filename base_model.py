from peewee import Model
from bd import bd

class BaseModel(Model):
    class Meta:
        database = bd