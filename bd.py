from peewee import SqliteDatabase

NOMBRE = 'obras_urbanas'
RUTA = f'./database/{NOMBRE}.db'

bd = SqliteDatabase(RUTA)