from peewee import AutoField, CharField, ForeignKeyField
from base_model import BaseModel
from comuna import Comuna

class Barrio(BaseModel):
    id = AutoField()
    nombre = CharField(max_length=100)
    comuna = ForeignKeyField(Comuna,backref='barrios')

    class Meta:
        table_name = 'barrios'

    def obtener_barrios():
        barrios = []
        barrios_en_bd = Barrio.select(Barrio.id, Barrio.nombre)
        for barrio in barrios_en_bd:
            plantilla = {
                'id': barrio.id,
                'nombre': barrio.nombre
            }
            barrios.append(plantilla)
        return barrios
    
    def obtener_barrio_por_nombre(nombre: str):
        try:
            barrio = Barrio.select().where(Barrio.nombre == nombre).first()
            print(f'ID: {barrio.id} - nombre: {barrio.nombre}')
            return barrio
        except AttributeError:
            print('VALOR NO ENCONTRADO')

    def obtener_barrio_por_id(id: int):
        try:
            barrio = Barrio.select().where(Barrio.id == id).first()
            return barrio
        except AttributeError:
            print('VALOR NO ENCONTRADO')