from peewee import AutoField, CharField
from base_model import BaseModel

class Etapa(BaseModel):
    id = AutoField()
    nombre = CharField(max_length=100)

    class Meta: 
        table_name = 'etapas'