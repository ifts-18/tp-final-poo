# Estructura de la BD

- la BD tiene que almacenar los datos de las Obras Públicas del GCBA.
- el administrador de las obras son las distintas Áreas Responsables y Ministerios del GCBA.
- a su vez, el administrador es quien inicia los proyectos

pasos:
- primero se crea el proyecto
    - se deben registrar valores en los campos tipo_obra, area_responsable y barrio
- segundo se licita la obra
    - se deben registrar valores en los campos tipo_contratacion y nro_contratacion
- tercero se adjudica la obra
    - se deben registrar valores en empresa y nro_expediente
- cuarto se debe iniciar la obra
    - se deben registrar valores en destacada, fecha_inicio, fecha_fin_inicial, financiamiento y mano_obra
- quinto se van registrando los avances en la obra
    - se actualiza el atributo porcentaje_avance
- sexto se finaliza la obra
    - se deben registrar valores en etapa y porcentaje_avance

adicionales:
- se puede incrementar el plazo de la obra
    - se debe actualizar el valor del atributo plazo_meses
- se puede incrementar la mano de obra
    - se debe actualizar el valor mano_obra
- se puede rescindir una obra
    - se debe actualizar el valor etapa


### borrador nuevas entidades

- obra
    - id
    - tipo_obra
    - area_responsable
    - ubicacion
        - barrio
            - comuna
    - etapa
    - destacada
    - fecha_inicio
    - fecha_fin_inicial
    - 
- licitacion
    - id
    - tipo_contratacion
    - nro_contratacion
- adjudicacion
    - id
    - empresa
    - nro_expediente



### Proceso de creación de una Obra Pública

- el área responsable crea un nuevo proyecto (crea una licitacion) --> etapa 'proyecto'
- en el proceso de licitación se asignan valores a:
    - tipo_obra
    - area_responsable
    - barrio


### Entidades

- obra
- contrato
- contratista
- area
- comuna
- barrio
- entorno


### Atributos

- obra:
    - id 
    - nombre
    - ubicacion
        - id
        - direccion
        - entorno
        - barrio
        - comuna
    - etapa
    - tipo_obra
    - area_responsable
    - descripcion
    - contrato
        - id
        - monto_contrato
        - fecha_inicio
        - fecha_fin
        - plazo_meses
        - contratista
            - id
            - cuit_contratista
            - razon_social (licitacion_oferta_empresa)
        - licitacion_anio
        - contratacion_tipo
        - nro_contratacion
        - expediente_numero
    - porcentaje_avance
    - mano_obra


### Columnas a importar del archivo csv

- entorno
- nombre
- etapa
- tipo
- area_responsable
- descripcion
- monto_contrato
- comuna
- barrio
- direccion
- fecha_inicio
- fecha_fin_inicial
- plazo_meses
- porcentaje_avance
- licitacion_oferta_empresa
- licitacion_anio
- contratacion_tipo
- nro_contratacion
- cuit_contratista
- mano_obra
- destacada
- expediente-numero
- financiamiento


### Relaciones:

- una ubicación puede tener cero obras como mínimo y muchas como máximo
- una obra es realizada como mínimo y cómo máximo en una ubicación

- una obra pertenece como mínimo y como máximo a un contrato
- un contrato tiene como mínimo y como máximo una obra

- un contrato tiene como mínimo un contratista y como máximo muchos contratistas
- un contratista puede tener cero contratos como mínimo y muchos como máximo

- un contratista puede realizar cero obras como mínimo y muchas obras como máximo
- una obra es realizada como mínimo y como máximo por un contratista

- un área puede gestionar cero obras como mínimo y muchas obras como máximo
- una obra es gestionada como mínimo y como máximo por un área

- una comuna puede tener cero obras como mínimo y muchas obras como máximo
- una obra es realizada como mínimo en una comuna y como máximo en muchas comunas

- un barrio puede tener cero obras como mínimo y muchas obras como máximo
- una obra es realizada como mínimo en un barrio y como máximo en muchos barrios

- una comuna tiene como mínimo y como máximo muchos barrios
- un barrio pertenece como mínimo y como máximo a una comuna

- un entorno puede pertenecer como mínimo a un barrio y como máximo a muchos barrios
- un barrio puede contener un entorno como mínimo y muchos entornos como máximo


### Cosas a limpiar de la BD

- sacar todos los registros que no tengan licitacion_anio de 4 enteros
- sacar todos los registros que no tengan un cuit en contratista